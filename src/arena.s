;;----------------------------------------------------------------------------------------------------------------------
;; Memory management
;;
;; This will manage 8K arenas
;;----------------------------------------------------------------------------------------------------------------------
;;
;; Arenas are allocate-only memory areas.  Because of the ZX Spectrum Next page size, 8K is the limit for any single
;; allocation.  An arena starts out as a single 8K block of memory whose first 228 bytes contains meta-data describing
;; the arena.  The initial bytes are as follows:
;;
;;      Offset  Size    Description
;;      0       224     The page indicies that comprise this arena.
;;      224     1       The number of pages in the arena
;;      225     3       The current 24-bit address offset into the arena of the next allocation.
;;      228             First free byte of arena
;;
;; Each page is reserved via NextZXOS, which will allocate the last physical page first and then the one before that,
;; and so on.  When an allocation is made, a check is made to see if it will fit on the latest page.  If not, a new
;; page is allocated to make room for it.  Allocations can NEVER cross page boundaries.  Any memory unused on the
;; previous page is wasted, so you have to be careful how you allocate.  What you will get in return is a 24-bit
;; address.  Bits 0-12 will be the offset into the page the allocation resides on (0-8191), and bits 16-23 will be
;; the page index of the allocation.  Alternatively, you can ask the memory manager to start allocating immediately
;; on the next page.
;;
;; When using the memory that has been allocated, it has to be prepared first.  This process pages in the correct page
;; so that it sits in the $c000-$dfff memory area ready for you to read or write to it.
;;
;; Finally, you can deallocating everything you've allocated in that arena by destroying the arena.
;;
;;----------------------------------------------------------------------------------------------------------------------
;; The interface:
;;
;;      arenaNew        Creates a new arena and returns a handle to that arena for use in other functions.
;;      arenaDone       Destroy an arena, giving back all allocated pages back to the OS.
;;      arenaAlign      Make sure the next allocation starts at the beginning of a new page and return its index.
;;      arenaAlloc      Allocate up to 8K bytes and return a 24-bit reference to that memory.
;;      arenaPrepare    Ensure that allocated memory is paged in so it is visible by the CPU.  Returns a real address.
;;
;;----------------------------------------------------------------------------------------------------------------------


; Arena meta-data addresses
; Pages are always paged into $c000-$dfff
ARENAHDR_PAGES          equ     $c000   ; Array of pages used in arena
ARENAHDR_NUMPAGES       equ     $c0e0   ; Number of pages allocated so far
ARENAHDR_NEXTOFFSET     equ     $c0e1   ; 16-bit offset (< 8K) where next allocation will be placed
ARENAHDR_NEXTPAGE       equ     $c0e3   ; 8-bit page # where next allocation will be
ARENAHDR_SIZE           equ     $e4

;;----------------------------------------------------------------------------------------------------------------------
;; Create a new arena.
;;
;; Ouput:
;;      A = handle to arena.  Use this when calling other routines.
;;      ZF = 1 if allocation failed, 0 otherwise.
;;
;;----------------------------------------------------------------------------------------------------------------------

arenaNew:
                push    de
                push    hl
                call    allocPage                       ; Allocate a page to hold the meta-data and the first allocations
                and     a                               ; Did we fail?
                jr      z,.fail
                ld      e,a                             ; Save the page #

                nextreg NR_MMU6,a                       ; Page it in to MMU 6 ($c000-$dfff)
                ld      a,1
                ld      (ARENAHDR_NUMPAGES),a           ; Store # of pages
                ld      hl,228
                ld      (ARENAHDR_NEXTOFFSET),hl        ; Initial offset for first allocation
                ld      a,e
                ld      (ARENAHDR_PAGES),a              ; Store first page # in chain
                ld      (ARENAHDR_NEXTPAGE),a           ; Store MSB of first allocation reference (its page #)

.fail           pop     hl
                pop     de
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; Destroy an arena and return all allocated pages back to the OS.
;;
;; Input:
;;      A = handle
;;
;; Output:
;;      A = handle
;;      ZF = 0
;;      CF = 0
;;
;;----------------------------------------------------------------------------------------------------------------------

arenaDone:
                push    bc
                push    hl

                ; Page in the first page of the chain to get access to meta-data
                nextreg NR_MMU6,a
                ld      a,(ARENAHDR_NUMPAGES)
                ld      b,a             ; # of pages in chain
                ld      h,$c0
                ld      l,a             ; HL = address of next page # location in array
                dec     l               ; HL now points to last entry in array

.l1             ld      a,(hl)          ; A = next page to free
                call    freePage        ; We assume that the NextZXOS function doesn't change paging
                dec     hl              ; Next page index location in array
                djnz    .l1

                pop     hl
                pop     bc
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; Ensure the next allocation starts at the beginning of a page.  The new page is paged in.
;;
;; Input:
;;      A = handle
;;
;; Output:
;;      A = new page # or 0 if failed
;;
;;----------------------------------------------------------------------------------------------------------------------

arenaAlign:
                push    de
                push    hl

                nextreg NR_MMU6,a       ; Page in first page of arena (with meta-data)
                ld      h,$c0           ; H = MMU 6
                call    allocPage       ; A = new page # or 0
                and     a
                jr      z,.no_mem

                ld      e,a             ; E = new page #
                ld      a,(ARENAHDR_NUMPAGES)
                ld      l,a             ; HL = new entry in page table
                ld      (hl),e          ; Add new page in page table
                inc     a               ; We have one more page!
                ld      (ARENAHDR_NUMPAGES),a
                ld      l,ARENAHDR_NEXTOFFSET % 256
                xor     a
                ld      (hl),a          ; Next free address is at beginning of page (offset 0)
                inc     hl
                ld      (hl),a
                inc     hl
                ld      (hl),e          ; Write 24-bit address for next allocation
                ld      a,e
                nextreg NR_MMU6,a       ; Page in the new page

.no_mem         pop     hl
                pop     de
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; Get current address
;;
;; Input:
;;      A = arena handle
;;
;; Output:
;;      A = page index
;;      HL = offset into page
;;
;;----------------------------------------------------------------------------------------------------------------------

arenaGetAddr:
                nextreg NR_MMU6,a       ; Page in first page of arena (with meta-data)

                ld      a,(ARENAHDR_NUMPAGES)
                dec     a               ; A = page index
                ld      hl,(ARENAHDR_NEXTOFFSET)
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; Push data to an arena.  This routine will cross boundaries.
;;
;; Input:
;;      A = arena handle
;;      DE = data (cannot be in MMU6: $c000-$dfff)
;;      BC = # of bytes (<= 8K)
;;
;; Output:
;;      CF = 1 if out of memory
;;----------------------------------------------------------------------------------------------------------------------

arenaPushData:
                push    af
                push    bc
                push    de
                push    hl
                push    af              ; Save handle

                nextreg NR_MMU6,a       ; Page in meta data for arena
                ld      hl,(ARENAHDR_NEXTOFFSET)
                add     hl,bc           ; Calculate final address
                ld      a,h
                and     $e0
                jr      z,.simple_copy  ; All of data fits into current page

                ; Split copy
                ld      a,h
                sub     $20             ; AL = length of second part, CF = 0
                ld      h,a             ; HL = length of second part (L2)

                ; Subtract HL from BC
                ;TODO
                swap16  hl,bc           ; BC = L2, HL = original length
                sbc     hl,bc           ; HL = L1

                ; Copy first half
                pop     af
                push    af              ; A = handle
                push    bc
                ld      bc,hl
                call    arenaAllocPrepare
                jr      c,.oom
                ex      de,hl           ; HL = data, DE = address to copy to
                call    memcpy
                add     hl,bc           ; Adjust start address

                ; Copy second half
                pop     bc              ; Continue into copy
                ex      de,hl           ; DE = data

.simple_copy:
                pop     af              ; Retrieve handle
                call    arenaAllocPrepare
                ex      de,hl
                call    memcpy

.end            pop     hl,de,bc,af
                and     a
                ret

.oom:
                pop     hl,de,bc,af
                scf
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; Push a single byte on to the arena
;;
;; Input:
;;      A = arena handle
;;      C = byte
;;
;; Output:
;;      CF = 1 if error
;;      HL = address where byte is written (if CF = 0)
;;
;;----------------------------------------------------------------------------------------------------------------------

arenaPushByte:
                push    bc
                ld      bc,1
                call    arenaAllocPrepare       ; HL = address to write byte
                pop     bc
                ret     c                       ; If an error ocurred, exit
                ld      (hl),c
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; Allocate memory.
;;
;; Input:
;;      A = arena handle
;;      BC = size
;;
;; Output:
;;      A = page index
;;      HL = offset into page
;;      CF = 1 if error (and AHL == 0)
;;
;;----------------------------------------------------------------------------------------------------------------------
;;
;; Test cases:
;;      1)      Allocate 0 bytes (expected to return same address of current next pointer)
;;      2)      Allocate 5000 bytes (expected to return same address of current next pointer and advance pointer)
;;      3)      Allocate 8000 bytes (expected to allocate new page)
;;      4)      Allocate 8192 bytes (expected to allocate new page, fill page)
;;      5)      Allocate 9000 bytes (expect return address of 0)
;;      6)      Allocate 8192 bytes, then 100 bytes (expected to allocate in new page, fill page, then allocate 100)
;;      7)      Allocate 8192 bytes, then 8192 bytes (expected to allocate 2 pages and fill them)
;;
;;----------------------------------------------------------------------------------------------------------------------

arenaAlloc:
                push    bc
                push    de

                ld      e,a                             ; E = handle
                nextreg NR_MMU6,a                       ; Page in first page of arena (with meta-data)

                ; Check to see if the size is <= 8K ($2000)
                ld      hl,(ARENAHDR_NEXTOFFSET)        ; HL = number of bytes used in latest page and LSW of allocation
                ld      a,b
                or      c
                jr      z,.zero                         ; Size == 0?
                dec     bc                              ; BC = size - 1
                ld      a,b
                and     $e0                             ; Top 3 bits should be 0 if <= 8K
                jr      nz,.too_large

                ; Obtain how much space on the current page
                push    hl                              ; Store allocation address
                add     hl,bc                           ; HL = number of bytes + size - 1
                ld      a,h
                and     $e0                             ; Final size <= 8K (final size - 1 < 8K)?
                jr      z,.alloc                        ; Yes, go ahead and allocate

                ; No? We need to allocate another page
                ld      a,e                             ; A = handle
                call    arenaAlign                      ; A = new page #
                pop     hl                              ; Throw away allocation address
                push    0                               ; New allocation address (beginning of page)
                ld      h,b
                ld      l,c                             ; HL = new offset - 1

.alloc          ; At this point:
                ;       HL = new offset - 1
                ;       Stack has old offset
                inc     hl                              ; CF = 0 after this.
                ld      a,e
                nextreg NR_MMU6,a                       ; Bring back the initial arena page
                ld      (ARENAHDR_NEXTOFFSET),hl        ; Store new offset
                ld      a,(ARENAHDR_NUMPAGES)
                dec     a                               ; A = page index
                pop     hl                              ; HL = LSW of allocation

                ; At this point, AHL = allocation address
.finish         pop     de
                pop     bc
                ret                                     ; AHL = allocation address

                ; This handles an allocation size that is too large (> 8K).  We can't allocate this much as it
                ; won't fit on a single page.  Return address of 0
.too_large      xor     a
                ld      h,a
                ld      l,a                             ; AHL = 0
                scf
                jr      .finish

                ; This handles size of zero.  HL is already the LSW of the allocation address.  Just need to fetch
                ; the MSB of the allocation address.
.zero           ld      a,(ARENAHDR_NUMPAGES)
                dec     a
                and     a                               ; Clear carry flag (since it will be 1 at this point)
                jr      .finish

;;----------------------------------------------------------------------------------------------------------------------
;; arenaAllocPrepare
;; Allocates as normal but then ensures that the new memory allocation is paged in and ready to use.  The page index
;; and page offset of the new allocation is lost.  But if you just want to write to the end of an arena and not store
;; the 24-bit address, this is useful.  Because the allocation is guaranteed to be inside an 8K page, you don't have
;; to worry about boundaries when writing to the bytes allocated.
;;
;; Input:
;;      A = arena page
;;      BC = number of bytes to allocate
;;
;; Output:
;;      HL = read address inside MMU6
;;      CF = 1 if out of memory
;;
;;----------------------------------------------------------------------------------------------------------------------

arenaAllocPrepare:
                push    de
                push    af              ; Store arena handle
                call    arenaAlloc      ; Do the allocation, A = index, HL = offset
                jr      c,.oom
                ld      e,a
                pop     af
                call    arenaPrepare
                pop     de
                and     a
                ret
.oom:
                pop     af,de
                scf
                ret
                
;;----------------------------------------------------------------------------------------------------------------------
;; Prepare a 24-bit address, by paging in the correct page at MMU 6.
;;
;; Input:
;;      A = arena page
;;      EHL = 24-bit address (E = page index, HL = offset into page)
;;
;; Output:
;;      HL = real address inside MMU 6 for data.
;;
;; Affected:
;;      A
;;
;;----------------------------------------------------------------------------------------------------------------------

arenaPrepare:
                nextreg NR_MMU6,a       ; Page in arena
                push    hl
                ld      hl,ARENAHDR_PAGES
                ld      a,e
                add     hl,a
                ld      a,(hl)
                pop     hl
                nextreg NR_MMU6,a
                ld      a,h
                add     a,$c0
                ld      h,a             ; HL = $C000 + offset
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; Prepare a 24-bit address, by paging in the correct pages at MMU6 and 7
;;
;; Input:
;;      A = arena page
;;      EHL = 24-bit address (E = page index, HL = offset into page)
;;
;; Output:
;;      HL = real address inside MMU6 for data.
;;
;; Affected:
;;      A, A'
;;
;;----------------------------------------------------------------------------------------------------------------------

arenaPrepare16K:
                nextreg NR_MMU6,a       ; Page in arena
                push    hl
                ld      hl,ARENAHDR_PAGES
                ld      a,e
                add     hl,a
                ldi     a,(hl)
                exa
                ld      a,(hl)
                pop     hl
                nextreg NR_MMU7,a
                exa
                nextreg NR_MMU6,a
                ld      a,h
                add     a,$c0
                ld      h,a             ; HL = $C000 + offset
                ret


;;----------------------------------------------------------------------------------------------------------------------
;; compressAddr
;; Convert an arena address (AHL) to a 16-bit address.  The arena cannot be greater than 64K.
;;
;; Input:
;;      A = page index (P) (0-7)
;;      HL = offset in page (O) (0-8K)
;;
;; Output:
;;      HL = 16-bit compressed address: PPPOOOOO OOOOOOOO
;;

compressAddr:
                swapnib         ; A = 0PPP0000
                add     a,a     ; A = PPP00000
                or      h
                ld      h,a     ; HL = PPPOOOOO OOOOOOOO
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; decompressAddr
;; Convert a compressed 16-bit address back to a 24-bit address (AHL).
;;
;; Input:
;;      HL = 16-bit compressed addres
;;
;; Ouput:
;;      A = page index (0-7)
;;      HL = page offet (0-8K)
;;

decompressAddr:
                push    de
                ld      a,h     ; A = PPPOOOOO
                and     $1f     ; A = 000OOOOO
                ld      d,a
                ld      a,h     ; A = PPPOOOOO
                swapnib         ; A = OOOOPPPO
                rrca
                and     7       ; A = 00000PPP
                ld      h,d     ; HL = 000OOOOO OOOOOOOO
                pop     de
                ret

;;----------------------------------------------------------------------------------------------------------------------
;;----------------------------------------------------------------------------------------------------------------------
