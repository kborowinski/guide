;;----------------------------------------------------------------------------------------------------------------------
;; Utilities that act on the Scratch buffer
;;----------------------------------------------------------------------------------------------------------------------

;;----------------------------------------------------------------------------------------------------------------------
;; fetchWord
;; Read a string and fetch the next word (series of characters, numbers and _) pointed to by DE.
;;
;; Input:
;;      H = start 256-byte page of buffer to fetch to (usually Scratch/256).
;;      DE = start of word
;;
;; Output:
;;      DE = points past word (also skip whitespace)
;;
;; Uses:
;;      A, L
;;----------------------------------------------------------------------------------------------------------------------

fetchWord:
                push    bc
                ld      l,0
                ld      a,(de)
                jr      .skipnumbers

.l1             ld      a,(de)

                bchilo  '0','9'+1               ; Numbers
                call    testRange
                jr      c,.accept

.skipnumbers:
        
                bchilo  'A','Z'+1
                call    testRange               ; Uppercase letter?
                jr      c,.accept
        
                bchilo  'a','z'+1               ; Lowercase letter?
                call    testRange
                jr      c,.accept

                cp      '_'
                jr      z,.accept

                ; Found the end of the word - skip any whitespace (32 and 9)
.skip           ld      a,(de)
                inc     de
                cp      ' '
                jr      z,.skip
                cp      9
                jr      z,.skip
                dec     de
                xor     a
                ld      (hl),a
                pop     bc
                ret
        
.accept:        ; This character is acceptable
                inc     de
                ld      (hl),a
                inc     l
                jr      nz,.l1

                ; Reached end of scratch
                dec     l
                jr      .skip

;;----------------------------------------------------------------------------------------------------------------------
;; fetchQuotedWord
;; Like fetchWord but will accept quotes.
;;
;; Input:
;;      H = start 256-byte page of buffer to fetch to (usually Scratch/256).
;;      DE = start of word
;;
;; Output:
;;      DE = points past word (also skip whitespace)
;;----------------------------------------------------------------------------------------------------------------------

fetchQuotedWord:
                ld      a,(de)
                cp      $22             ; Check for quotes "
                jp      nz,fetchWord
                push    bc

                ld      l,0
                inc     de              ; Skip past quotes

.l1             ld      a,(de)
                inc     de
                cp      $22             ; Found last quote
                jr      z,fetchWord.skip

                ld      (hl),a          ; Copy character
                inc     l
                jr      nz,.l1          ; Reached end of scratch?  No, keep going

                dec     l
                jr      fetchWord.skip

;;----------------------------------------------------------------------------------------------------------------------
;; strUpper
;; Ensure letters in page-aligned buffer (up until 0 or run out of scratch) are all uppercase
;;
;; Input:
;;      H = start 256-byte page of buffer to fetch to (usually Scratch/256).
;;
;; Uses:
;;      L
;;

scratchStrUpper:
                ld      l,0
                push    af
.l1             ld      a,(hl)
                and     a
                jr      z,.end
                cp      'a'
                jr      c,.not_lc
                cp      'z'+1
                jr      nc,.not_lc
                sub     32
                ld      (hl),a
.not_lc:
                inc     l
                jr      nz,.l1
.end:
                pop     af
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; matchWord
;; Given a word table (that consists of an array of word size (1 byte) and the word (n bytes)), find the word that
;; matches the word in the page-aligned buffer.  The table should terminate with a words size of 0.
;;
;; Input:
;;      H = start 256-byte page of buffer to match to (usually Scratch/256).
;;      DE = Word table
;;
;; Output:
;;      A = index (if a match)
;;      CF = 0 if there is no match, 1 if there is.
;;
;; Uses:
;;      A
;;

matchWord:
                push    bc
                push    de
                push    hl
                ld      c,0             ; Current index

.next_word:
                ldi     a,(de)          ; Fetch length of next word
                and     a               ; There are no more words?
                jr      z,.end          ; No, end and CF = 0
                ld      b,a             ; B = length of word

                ld      l,0

.l1:
                ld      a,(de)
                cp      (hl)
                jr      nz,.no_match
                inc     l
                inc     de
                djnz    .l1

                ; If we reach this point, there is match
                ld      a,c             ; A = index
                scf

.end            pop     hl
                pop     de
                pop     bc
                ret

.no_match:      ld      a,b
                add     de,a            ; DE points to next word in command table
                inc     c               ; Increase index
                jr      .next_word
