@echo off
\env\sjasmplus\sjasmplus src\guide.s --zxnext=cspect --msg=war --fullpath
if not errorlevel 1 (
    copy /b guidebase+guidedisplay+font guide
    del guidebase
    del guidedisplay
    del font
    \env\hdfmonkey\hdfmonkey mkdir \env\tbblue.mmc docs
    \env\hdfmonkey\hdfmonkey mkdir \env\tbblue.mmc docs/guides
    \env\hdfmonkey\hdfmonkey put \env\tbblue.mmc etc/autoexec.bas nextzxos
    \env\hdfmonkey\hdfmonkey put \env\tbblue.mmc guide dot/
    \env\hdfmonkey\hdfmonkey put \env\tbblue.mmc data/test.gde /docs/guides/
    \env\hdfmonkey\hdfmonkey put \env\tbblue.mmc data/NextGuide.gde /docs/guides/
)
